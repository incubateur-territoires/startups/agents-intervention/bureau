const getSlugOfStatus = async function(event) {
    const { apiUrl } = useRuntimeConfig();
    const { slug } = event.context.params;
  
    return await $fetch(apiUrl + 'status/' + slug, {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': event.req.headers.authorization
      }
    });
  }
  
  export default defineEventHandler(getSlugOfStatus);
  
const getInterventions = async function (event: Event) {
  const { apiUrl } = useRuntimeConfig();

  const interventions = await $fetch(apiUrl + 'interventions', {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'authorization': event.req.headers.authorization
    }
  })
    .catch((error) => {
      return error;
    });

  return interventions['hydra:member'];
}

export default defineEventHandler(getInterventions);

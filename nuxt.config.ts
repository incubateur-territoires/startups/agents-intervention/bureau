// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    css: [
        '@gouvfr/dsfr/dist/dsfr/dsfr.min.css',
        '@gouvfr/dsfr/dist/utility/icons/icons-buildings/icons-buildings.min.css',
        '@gouvfr/dsfr/dist/utility/icons/icons-business/icons-business.min.css',
        '@gouvfr/dsfr/dist/utility/icons/icons-document/icons-document.min.css',
        '@gouvfr/dsfr/dist/utility/icons/icons-map/icons-map.min.css',
        '@gouvfr/dsfr/dist/utility/icons/icons-system/icons-system.min.css',
        '@gouvfr/dsfr/dist/utility/icons/icons-user/icons-user.min.css',
        '@vuepic/vue-datepicker/dist/main.css',
        'assets/global.css',
        'remixicon/fonts/remixicon.css'
    ],
    devtools: { enabled: true },
    runtimeConfig: {
        apiUrl: '',
        public: {
          publicApiUrl: ''
        }
    },
    typescript: {
        // typeCheck: true,
        strict: true
    },
    vite: {
        server: {
            hmr: {
                clientPort: 24679
            }
        }
    },
    build: {
        transpile: ['@vuepic/vue-datepicker']
    },
    modules: [
        'floating-vue/nuxt'
    ]
})

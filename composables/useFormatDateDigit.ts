export const useFormatDateDigit = (dateString: string) => {
    const options = { day: '2-digit', month: '2-digit', year: '2-digit' };
    const date = new Date(dateString);
    const currentDate = new Date();
    if (
        date.getDate() === currentDate.getDate() &&
        date.getMonth() === currentDate.getMonth() &&
        date.getFullYear() === currentDate.getFullYear()
    ) {
        return "aujourd'hui";
    } else if (date.getDate() === currentDate.getDate() - 1) {
        return "hier";
    } else {
        const formattedDate = date.toLocaleDateString('fr-FR', options).replace(/\//g, '/');
        return formattedDate;
    }
}    
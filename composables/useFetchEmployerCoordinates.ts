import jwt_decode from "jwt-decode";

export const useFetchEmployerCoordinates = async () => {
    const { baseURL } = useRuntimeConfig().app;
    const tokenCookie = useCookie("aei-desktop-token");
    const decodedToken = jwt_decode(tokenCookie.value);

    const {data: employer } = await useFetch(
        baseURL + "api/employers/" + decodedToken.employerId,
        {
            headers: useAuthorizationHeader("application/json"),
        }
    );

    return {
        longitude: employer.value.longitude,
        latitude: employer.value.latitude
    };
}
